const Hapi = require('hapi');
const Boom = require('boom');
const server = Hapi.server({
    host: 'localhost',
    port: 8003,
    routes: {
        cors: true

    }
});

const launchServer = async function() {

    const dbOpts = {
        url: 'mongodb://localhost:9999/user2',
        settings: {
            poolSize: 10
        },
        decorate: true
    };

    await server.register({
        plugin: require('hapi-mongodb'),
        options: dbOpts
    });

    server.route({
        method: 'GET',
        path: '/',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;

            try {
                const result = await db.collection('user').find().toArray();
                console.log(result)
                return result;

            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    });

    await server.start();
    console.log(`Server started at ${server.info.uri}`);
};

launchServer().catch((err) => {
    console.error(err);
    process.exit(1);
});